#!/usr/bin/env node

const fs = require("fs");
const child_process = require("child_process");
const process = require("process");

// Loads a cvs file with 1 row per line and tab as separator as an array of array
function loadCVSasArray(spreadsheet) {
  return ("" + fs.readFileSync(spreadsheet)).split("\n").map((l) => l.split("\t"));
}

// Extracts a section of a pdf
function pdfExtract(book, chapter, firstpage, lastpage) {
  if (firstpage > lastpage)
    console.log(`Error '${err}' running 'pdftk ${book} cat ${firstpage}-${lastpage} output ${chapter}' with spurious page numbers`);
  if (!fs.existsSync(`txt/${chapter}.txt`))
    child_process.execSync(`pdftk ${book} cat ${firstpage}-${lastpage} output pdf/${chapter}.pdf ; pdftotext pdf/${chapter}.pdf txt/${chapter}.txt`, (err, stdout, stderr) => {
    if (err || stderr != "") {
      console.log(`Error '${err}' running 'pdftk ${book} cat ${firstpage}-${lastpage} output ${chapter}', stderr: '${stderr}'`);
    }
    if (stdout != "")
      console.log(stdout);
  });
}


word_counts = { };

words_excluded = [ "the", "and", "of", "to", "that", "in", "he", "shall", "for", "his", "unto", "they", "be", "is", "them", "him", "not", "with", "it", "all", "thou", "was", "thy", "which", "their", "my", "but", "me", "have", "as", "will", "thee", "from", "ye", "are", "were", "when", "upon", "this", "by", "you", "there", "then", "her", "had", "into", "so", "we", "on", "also", "an" ];

data_output = { 'chapters': {}};

// Splits a txt/chapter.txt document into a sequence and set of words
function wordSplit(chapter) {
  try {
    let word_sequence = ("" + fs.readFileSync(`txt/${chapter}.txt`))
	.toLocaleLowerCase()
	.replace(new RegExp("['´][a-z]+", "g"), "")
	.replace(new RegExp("(\\s|[^a-z])+", "g"), "\n")
	.split("\n")
	.filter((word) => word.length > 1)
	.filter((word) => !(word in words_excluded));
    word_sequence.map((w) => word_counts[w] = (w in word_counts ? word_counts[w] : 0) + 1);
    let word_set = Array.from(new Set(word_sequence)).toSorted();
    const minimal_number_of_words = 20;
    if (word_set.length >= minimal_number_of_words) {
      data_output['chapters'][chapter] = {};
      data_output['chapters'][chapter]['sequence'] = word_sequence;
      data_output['chapters'][chapter]['set'] = word_set;
      data_output['chapters'][chapter]['set-length'] = word_set.length;
      data_output['chapters'][chapter]['sequence-length'] =  word_sequence.length;
    }
  } catch(err) {
    console.log(err);
  }
}

// Splits according to the t.o.c
let toc = loadCVSasArray("kjv.chapters.csv");
let offset = 49;
for(let ii = 0; ii < toc.length-1; ii++) {
  if (Number(toc[ii][1]) < Number(toc[ii+1][1])) {
    pdfExtract("kjv.pdf", toc[ii][0], offset + Number(toc[ii][1]), offset + Number(toc[ii+1][1]) - 1);
    wordSplit(toc[ii][0]);
  }
}
// Sorts the word counts
word_counts = Object.fromEntries(Object.entries(word_counts).sort(([,a],[,b]) => b-a));

data_output[ 'word-counts'] = word_counts;
fs.writeFileSync(`kjv.data.json`, JSON.stringify(data_output, null, 2));



