Applying inference at depth 0 on a relation map of size 0
Added triples: output: {
  [ Luigieats thisPizza ]
  [ eatsrdfs:domain Person ]
  [ eatsrdfs:range Food ]
  [ thisPizzardf:type MagheritaPizza ]
  [ thisPizzahasTopping thisMozzarella ]
  [ MagheritaPizzardfs:subClassOf Pizza ]
  [ Pizzardfs:subClassOf Food ]
  [ hasToppingrdfs:subPropertyOf hasIngredient ]
  [ MagheritaPizzahasTopping Tomato ]
}_<3±0.085>

Applying inference at depth 1 on a relation map of size 9
[ thisPizza rdf:type MagheritaPizza ]_<1±0.0094> ^ [ MagheritaPizza rdfs:subClassOf Pizza ]_<1±0.0094> =rdfs9_<1±2.3e-08> => 
[ thisPizza rdf:type MagheritaPizza ]_<1±0.0094> ^ [ MagheritaPizza rdfs:subClassOf Pizza ]_<1±0.0094> =rdfs9_<1±2.3e-08> => 
Added triples: output: {
  [ thisPizzardf:type_<1±2.3e-08> Pizza ]
}_<1±0.0094>

Applying inference at depth 2 on a relation map of size 10
[ thisPizza rdf:type_<1±2.3e-08> Pizza ]_<1±0.0094> ^ [ Pizza rdfs:subClassOf Food ]_<1±0.0094> =rdfs9_<1±4.5e-08> => 
Added triples: output: {
  [ thisPizzardf:type_<1±4.5e-08> Food ]
}_<1±0.0094>

Applying inference at depth 3 on a relation map of size 11
Added triples: output: {
}

Calculus duration in msec : 12.081
"Luigi pizza infered facts": {
  [ Luigieats thisPizza ]
  [ eatsrdfs:domain Person ]
  [ eatsrdfs:range Food ]
  [ thisPizzardf:type MagheritaPizza ]
  [ thisPizzahasTopping thisMozzarella ]
  [ MagheritaPizzardfs:subClassOf Pizza ]
  [ Pizzardfs:subClassOf Food ]
  [ hasToppingrdfs:subPropertyOf hasIngredient ]
  [ MagheritaPizzahasTopping Tomato ]
  [ thisPizzardf:type_<1±2.3e-08> Pizza ]
  [ thisPizzardf:type_<1±4.5e-08> Food ]
}_<3.3±0.1>

Calculus duration in msec : 5.375
"Luigi pizza infered facts": {
  [ Luigieats_<0.5±0> thisPizza ]
  [ eatsrdfs:domain Person ]
  [ eatsrdfs:range Food ]
  [ thisPizzardf:type MagheritaPizza ]
  [ thisPizzahasTopping thisMozzarella ]
  [ MagheritaPizzardfs:subClassOf Pizza ]
  [ Pizzardfs:subClassOf Food ]
  [ hasToppingrdfs:subPropertyOf hasIngredient ]
  [ MagheritaPizzahasTopping Tomato ]
  [ thisPizzardf:type_<1±2.3e-08> Pizza ]
  [ thisPizzardf:type_<1±4.5e-08> Food ]
}_<3.2±0.099>

