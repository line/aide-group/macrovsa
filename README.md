# macrovsa

Implements a macroscopic algorithmic ersatz of VSA some operations

@aideAPI

### This only an alpha version, better contact us to discuss, what it is and how to use it :)

The usual Vector Symbolic Architecture are implemented at microscopic using, e.g., the Neural Engineering Framework allowing a microscopic simulation of the neuronal processes, at the spiking neural network level. At a higher scale, when implemented using Semantic Pointer Architecture, based on linear algebra and permutation operations, thus at a mesoscopic scale, it allows to perform the same operations, but without explicitizing the neural state value and evolution.

A step further, at a higher macroscopic scale, we could directly consider the previous operations predicting the result of the different algebraic operations without explicitly calculating on vector components. This could be called an ``algorithmic ersatz'', and this is what is implemented here.

Please refer to the draft in submission regarding:
- [Algorithmic ersatz for VSA](https://www.overleaf.com/read/pmjznjqsctym)
  - with the related [numerical results](./macrovsa_experiments.pdf)
  - and related [inference mechanism trace](small_pizza_experiments.out.txt).
- [Biologically plausible reasoning embedded in neuronal computation](https://www.overleaf.com/read/kjwbnzkpvdxq#09e17b)
  - with the related [inference mechanism trace](pizza_experiments.out.txt).





<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/macrovsa'>https://gitlab.inria.fr/line/aide-group/macrovsa</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/macrovsa'>https://line.gitlabpages.inria.fr/aide-group/macrovsa</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/macrovsa/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/macrovsa/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/macrovsa'>softwareherirage.org</a>
- Version `0.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/macrovsa.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>aidesys: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>Basic system C/C++ interface routines to ease multi-language middleware integration</a></tt>
- <tt>wjson: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/wjson'>Implements a JavaScript JSON weak-syntax reader and writer</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
- Chloé Mercier&nbsp; <big><a target='_blank' href='mailto:chloe.mercier@inria.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www.linkedin.com/in/chl-mercier/'>&#128463;</a></big>
