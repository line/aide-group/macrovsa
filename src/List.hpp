#ifndef __macrovsa_List__
#define __macrovsa_List__

#include "AssociativeMap.hpp"
#include "Number.hpp"

namespace macrovsa {
  /**
   * @class List
   * @description Implements a macroscopic ersatz of a VSA chained list.
   * - A chained list is created inserting current-value/next-value pairs of symbol.
   * - It is implemented via [Bundling](./Bundling.html) and [Binding](./Binding.html), without using a C++ `std::list`.
   * @extends AssociativeMap
   */
  class List: public AssociativeMap {
public:

    /**
     * @function add
     * @memberof List
     * @instance
     * @description Inserts a new symbol to the container.
     * - Adding twice the same current-value/next-value pair corresponds to adding their belief level `tau`.
     * @param {uint} current The symbol after which the value is inserted.
     * @param {Symbol} value The symbol value to insert.
     * @return {List} A reference to this associative map, allowing to chain add methods.
     */
    List& add(const Symbol& current, const Symbol& value);

    /**
     * @function erase
     * @memberof List
     * @instance
     * @description Erases a symbol in the chained.
     * - Adding twice the same current-value/next-value pair corresponds to adding their belief level `tau`.
     * @param {uint} current The symbol after which the value is inserted.
     * @param {Symbol} value The symbol value to insert.
     * @return {List} A reference to this associative map, allowing to chain add methods.
     */
    List& erase(const Symbol& current, const Symbol& value);

    /**
     * @function getNext
     * @memberof List
     * @instance
     * @description Returns the next element after a given symbol.
     * - It is a simple synonym of the get() function.
     * @param {uint} current The symbol current value next which element is to return.
     * @return {Symbol} value A reference to the stored value, available until program end.
     */
    const Symbol& getNext(const Symbol& current) const;

    /**
     * @function getPrevious
     * @memberof List
     * @instance
     * @description Returns the previous element before a given symbol.
     * - This function is of `O(list size)`, for this implementation, since it must scan the list.
     * @param {uint} current The symbol current value next which element is to returnb.
     * @return {Symbol} value A reference to the stored value, available until program end.
     */
    const Symbol& getPrevious(const Symbol& current) const;
  };
}

#endif
