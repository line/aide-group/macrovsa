#ifndef __macrovsa_Binding__
#define __macrovsa_Binding__

#include "Symbol.hpp"

namespace macrovsa {
  /**
   * @class Binding
   * @description Implements a macroscopic ersatz of a VSA left-binding/unbinding.
   * - Implements the VTB `B(y) x` and `B(y~) x` operators, i.e., either
   *    - the binding of `x` by the `VTB` matrix of `y`,
   *    - the unbinding of `x` by the `VTB` matrix of `y~`, with `B(y~) = B(y)^T`.
   * - The binding name `B(y)_x` is calculated from its member name.
   * - The binding belief `tau = tau_y tau_x` and `sigma = sigma_0 + sigma_y + sigma_x` is calculated from its member belief.
   * - Note: a binding can not be copied, use the [clone()](Symbol.html/#.clone) method instead.
   * @extends Symbol
   * @param {Symbol} y The y symbol.
   * @param {Symbol} x The x symbol.
   * @param {bool} [b=true] If true constructs a binding, if false construct an unbinding.
   * @param {bool} [normalized=false] If true the mesoscopic computed vector is normalized to a unary magnitude.
   */
  class Binding: public Symbol {
    // Non assignable
    Binding(Binding const&) = delete;
    Binding& operator = (Binding const&) = delete;
    // Symbol remanence
    Symbols symbols;
    // Internal variable
    const Symbol& y_, & x_;
    bool b_, normalized_;
    // Used only by the clone function
    friend class Symbol;
    Binding(const Binding& symbol, Symbols & symbols);
protected:
    // Mesoscopic implementation
    virtual void setVector(double *vector);
public:
    Binding(const Symbol& y, const Symbol& x, bool b = true, bool normalized = false);
    virtual ~Binding() {}
    virtual bool equals(const Symbol& symbol, char what = 'i') const;
    virtual std::string asString() const;

    /**
     * @function y
     * @memberof Binding
     * @instance
     * @description Returns the binding operator y left parameter.
     * @return {Symbol} The y value.
     */
    const Symbol& y() const
    {
      return y_;
    }
    /**
     * @function x
     * @memberof Binding
     * @instance
     * @description Returns the binding operator x left parameter.
     * @return {Symbol} The x value.
     */
    const Symbol& x() const
    {
      return x_;
    }
    /**
     * @function b
     * @memberof Binding
     * @instance
     * @description Returns the binding/unbinding flag.
     * @return {bool} If true the operation is a bindind, if false it is an unbinding.
     */
    const bool b() const
    {
      return b_;
    }
    /**
     * @function normalized
     * @memberof Binding
     * @instance
     * @description Returns the normalize flag.
     * @return {bool} If true mesoscopic computed vector is normalized to a unary magnitude.
     */
    const bool normalized() const
    {
      return normalized_;
    }
    /**
     * @function asString
     * @memberof Binding
     * @instance
     * @description Returns the value as a string.
     * @return {String} A string of the `B_(y)(x)_<belief>` for binding or `B_(y~)(x)_<belief>` for unbinding, omitting the belief if `tau=1, sigma=0`.
     */
  };
}

#endif
