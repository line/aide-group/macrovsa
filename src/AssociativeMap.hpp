#ifndef __macrovsa_AssociativeMap__
#define __macrovsa_AssociativeMap__

#include "Symbol.hpp"
#include "Bundling.hpp"
#include "Binding.hpp"

namespace macrovsa {
  /**
   * @class AssociativeMap
   * @description Implements a macroscopic ersatz of a VSA associative map.
   * - An associative map is created adding new key/value pairs of symbol.
   * - It is implemented via [Bundling](./Bundling.html) and [Binding](./Binding.html), without using a C++ `std::map`.
   * @extends Bundling
   * @param {string} [name] An optional name. By default, a `#index` unique name is generated.
   */
  class AssociativeMap: public Bundling {
public:
    AssociativeMap() {}
    AssociativeMap(String name) : Bundling(name) {}

    /**
     * @function add
     * @memberof AssociativeMap
     * @instance
     * @description Adds a new symbol pair to the container.
     * - Adding twice the same key/value pair corresponds to adding their belief level `tau`.
     * @param {Symbol} key The symbol key to add or modify.
     * @param {Symbol} value The symbol value to add.
     * @return {AssociativeMap} A reference to this associative map, allowing to chain add methods.
     */
    AssociativeMap& add(const Symbol& key, const Symbol& value);

    /**
     * @function erase
     * @memberof AssociativeMap
     * @instance
     * @description Erases a symbol in the container.
     * @param {Symbol} [key] The symbol key to add or modify.
     *   - Without key argument erases all keys.
     * @return {AssociativeMap} A reference to this associative map, allowing to chain add methods.
     */
    AssociativeMap& erase(const Symbol& key);
    AssociativeMap& erase();

    /**
     * @function get
     * @memberof AssociativeMap
     * @instance
     * @description Returns an approximate value of the associative map by unbinding.
     * - For a map with `>_i B_(y_i) x_i` returns
     *    - `x_i + unknown` where `unknown` is a symbol orthogonal to all other symbols, if `y_i` is a map key.
     *    - `unknown` where `unknown` is a symbol orthogonal to all other symbols, otherwise.
     * - The result is reduced with respect to binding/unbinding operations.
     * @param {Symbol} key The symbol key `y_i`.
     * @return {Symbol} value A reference to the stored value `x_i + unknown`, available until program end.
     */
    const Symbol& get(const Symbol& key) const;

    /**
     * @function get
     * @memberof AssociativeMap
     * @instance
     * @description Defines an iterator over the associative map symbols, used in a construct of the form:
     * ```
     *  for(auto it = associativeMap.Bundling::get().cbegin(); it != associativeMap.Bundling::get().cend(); it++) {
     *    const Binding& binding = dynamic_cast < const Binding& > (it->second);
     *    const Symbol& key = binding.y(), value = binding.x();
     *    ../..
     *  }
     * ```
     * @return A `const std::map < Symbol* >&` reference for associative map iteration.
     */

    /**
     * @function asString
     * @memberof AssociativeMap
     * @instance
     * @description Returns the value as a string.
     * @return {String} A string of the form `{ key: value ...}_<belief>`, omitting the belief if `tau=1, sigma=0`.
     */
    virtual std::string asString() const;
  };
}

#endif
