 #include "Bundling.hpp"
 #include "AssociativeMap.hpp"
#include <set>

namespace macrovsa {
  Bundling::Bundling(const Bundling& symbol, Symbols& symbols) : Symbol(symbol.getName(), bundling)
  {
    for(auto it = symbol.get().cbegin(); it != symbol.get().cend(); it++) {
      add(clone(it->second, symbols));
    }
  }
  Bundling::~Bundling()
  {
    delete sortedmap;
  }
  bool Bundling::equals(const Symbol& symbol, char what) const
  {
    if(symbol.data.symbolType == bundling) {
      const Bundling& s = dynamic_cast < const Bundling& > (symbol);
      std::set < unsigned int > ids;
      for(auto it = get().cbegin(); it != get().cend(); it++) {
        ids.insert(it->first);
      }
      for(auto it = s.get().cbegin(); it != s.get().cend(); it++) {
        ids.insert(it->first);
      }
      for(auto it = ids.cbegin(); it != ids.cend(); it++) {
        auto jt = get().find(*it), kt = s.get().find(*it);
        if(jt == get().end() || kt == s.get().end()) {
          return false;
        }
        if(!get().at(*it).equals(s.get().at(*it), what)) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  Bundling& Bundling::add(const Symbol& symbol)
  {
    if(fabs(symbol.belief.tau) > 2 * symbol.belief.sigma) {
      if(symbol.data.symbolType == bundling) {
        const Bundling& s = dynamic_cast < const Bundling& > (symbol);
        for(auto it = s.get().cbegin(); it != s.get().cend(); it++) {
          add(it->second);
        }
      } else {
        auto it = values.find(symbol.getID());
        if(it == values.end()) {
          values.insert(std::pair < unsigned int, Symbol & > (symbol.getID(), clone(symbol, symbols)));
          // Updates tau
          {
            tau2 += symbol.belief.tau * symbol.belief.tau;
            // count++, belief.tau = ((count - 1) * belief.tau + sqrt(tau2)) / count;
            belief.tau = sqrt(tau2);
          }
          belief.sigma += symbol.belief.sigma;
        } else {
          // Updates tau
          {
            tau2 -= it->second.belief.tau * it->second.belief.tau;
            it->second.belief.tau += symbol.belief.tau;
            tau2 += it->second.belief.tau * it->second.belief.tau;
            // count++, belief.tau = ((count - 1) * belief.tau + sqrt(tau2)) / count;
            belief.tau = sqrt(tau2);
          }
          if(fabs(it->second.belief.tau) < it->second.belief.sigma) {
            values.erase(it->first);
          } else {
            it->second.belief.sigma += symbol.belief.sigma;
            belief.sigma += symbol.belief.sigma;
          }
        }
      }
    } else {
      belief.sigma += symbol.belief.sigma;
    }
    return *this;
  }
  const AssociativeMap& Bundling::getSorted() const
  {
    if (sortedmap == NULL)
      sortedmap = new AssociativeMap();
    else
      sortedmap->erase();
    struct symbol_order {  bool operator()(Symbol* a, Symbol* b) const  { return a->belief.tau  > b->belief.tau; } };
    std::set<const Symbol*,  symbol_order> symbols;
    for(auto it = get().cbegin(); it != get().cend(); it++) {
      symbols.insert(&it->second);
    }
    return *sortedmap;
  }
  Bundling& Bundling::erase(const Symbol& symbol)
  {
    values.erase(symbol.getID());
    return *this;
  }
  Bundling& Bundling::erase()
  {
    values.clear();
    return *this;
  }
  void Bundling::setVector(double *vector)
  {
    for(unsigned int i = 0; i < dimension; i++) {
      vector[i] = 0;
    }
    for(auto it = get().cbegin(); it != get().cend(); it++) {
      const Symbol& symbol = it->second;
      const double *vector_n = symbol.getVector();
      for(unsigned int i = 0; i < dimension; i++) {
        vector[i] += vector_n[i];
      }
    }
  }
  std::string Bundling::asString() const
  {
    std::string result = "[";
    for(auto it = get().cbegin(); it != get().cend(); it++) {
      const Symbol& symbol = it->second;
      result += " " + symbol.asString();
    }
    return result + " ]" + asStringTail();
  }
}
