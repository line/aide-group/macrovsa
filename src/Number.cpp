#include "Number.hpp"
#include "Binding.hpp"
#include "algo.hpp"
#include <vector>

namespace macrovsa {
  // Storage of the numbers as a unique class member for a proper memory liberation at program end
  class Numbers {
    std::vector < Symbol * > values;
public:
    const Symbol& getInt(unsigned int value)
    {
      static const unsigned int MAX_REASONNABLE_VALUE = 10000;
      if(value > values.size()) {
        if(value == 0) {
          values[0] = new Symbol("0");
        } else {
          aidesys::alert(value > MAX_REASONNABLE_VALUE, " illegal-argument", "in macrovsa::Number::getInt you are requesting a rather huge integer value %d > %d for such symbolic implementation, memory overflow may occur", value, MAX_REASONNABLE_VALUE);
          // Generates all numbers until the required value
          for(unsigned int n = values.size(); n <= value; n++) {
            values[n] = new Binding(aidesys::echo ("%d", value), getInt (n - 1));
          }
        }
      }
      return *values.at(value);
    }
    ~Numbers() {
      for(auto it = values.begin(); it != values.end(); it++) {
        delete(*it);
      }
    }
  }
  numbers;
  const Symbol& Number::getInt(unsigned int value)
  {
    return numbers.getInt(value);
  }
}
