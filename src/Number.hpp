#ifndef __macrovsa_Number__
#define __macrovsa_Number__

#include "Symbol.hpp"

namespace macrovsa {
  /**
   * @class Number
   * @description Implements a macroscopic ersatz of a VSA positive integer.
   * - It is implemented via successive [Binding](./Binding.html), without using a C++ `std::list`.
   */
  class Number {
public:

    /**
     * @function getInt
     * @memberof Number
     * @static
     * @description Returns the Symbol corresponding to a given positive integer.
     * @param {uint} value The symbol numeric value. Reasonnable value is between 0 and about 1000.
     * @return {Symbol} A reference to the corresponding value.
     */
    static const Symbol& getInt(unsigned int value);
  };
}

#endif
