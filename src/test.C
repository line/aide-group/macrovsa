#include "macrovsa.hpp"

using namespace macrovsa;

int main()
{
  // Tests the basic belief, symbol, bundling, binding, and reduce, sim mechanisms via an associative map
  {
    Symbol y("y"), x("x"), z("z");
    AssociativeMap map("test1");
    map.add(x, y).add(x, z).add(y, x);
    const Symbol& m_x = map.get(x);
    Belief x_m_x = algo::sim(x, m_x), y_m_x = algo::sim(y, m_x);
    aidesys::alert(x_m_x.tau != 0 || y_m_x.tau != 1, " illegal-state", "in macrovsa/test spurious result on basic data structures (x: '" + x.asString() + "' · map[x]: '" + m_x.asString() + "') = '" + x_m_x.asString() + "' (y: '" + y.asString() + "' · map[x]) = '" + y_m_x.asString() + "'");
  }
  // Tests the vector mesoscopic mechanisms
  {
    Symbol y("y"), x("x"), z("z");
    AssociativeMap map("test2");
    map.add(x, y).add(x, z).add(y, x);
    const Symbol& m_x = map.get(x);
    double x_m_x = algo::msim(x, m_x), y_m_x = algo::msim(y, m_x);
    aidesys::alert(fabs(x_m_x) > 0.1 || fabs(y_m_x) < 0.9, " illegal-state", "in macrovsa/test spurious on basic mesoscopic mechanism : (x · map[x]) = %f (y · map[x]) = %f", x_m_x, y_m_x);
  }
  // Tests the JSON syntax input output
  {
    const Symbol& symbol = Symbol::fromJSON("[ {b y: c x: [a b]}]");
    aidesys::alert(Symbol::toJSON(symbol) != Symbol::toJSON(Symbol::fromJSON(Symbol::toJSON(symbol))), " illegal-state", "in macrovsa/test spurious getSymbol/asString correspondance " + Symbol::toJSON(symbol) + " != " + Symbol::toJSON(Symbol::fromJSON(Symbol::toJSON(symbol))));
  }
  //
  return 0;
}
