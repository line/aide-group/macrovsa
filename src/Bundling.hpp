 #ifndef __macrovsa_Bundling__
#define __macrovsa_Bundling__

#include "Symbol.hpp"
#include <map>

namespace macrovsa {
  class AssociativeMap;
  /**
   * @class Bundling
   * @description Implements a macroscopic ersatz of a VSA bundling.
   * - A bundling or superposition is created adding new symbols.
   * - The bundling belief `tau = tau_1 tau_2 ...` and `sigma = sigma_1 + sigma_2 + ...` is calculated from its member belief.
   * - Note: a bundling can not be copied, use the [clone()](Symbol.html/#.clone) method instead.
   * @extends Symbol
   * @param {string} [name] An optional name. By default, a `#index` unique name is generated.
   * @param {...Symbol} symbol Symbol added to the bundling from 0 to 8 in this implementation
   */
  class Bundling: public Symbol {
    // Non assignable
    Bundling(Bundling const&) = delete;
    Bundling& operator = (Bundling const&) = delete;
    // Used only by the clone function
    friend class Symbol;
    Bundling(const Bundling& symbol, Symbols & symbols);
    std::map < unsigned int, Symbol & > values;
    // Used for the tau computation
    double count = 0, tau2 = 0;
protected:
    // Symbol remanence
    mutable Symbols symbols;
    // Mesoscopic implementation
    virtual void setVector(double *vector);
public:
    Bundling() : Symbol("", bundling) {}
    Bundling(String name) : Symbol(name, bundling) {}
    Bundling(String name, const Symbol& s1) : Symbol(name, bundling) {
      add(s1);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2) : Symbol(name, bundling) {
      add(s1), add(s2);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2, const Symbol& s3) : Symbol(name, bundling) {
      add(s1), add(s2), add(s3);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4) : Symbol(name, bundling) {
      add(s1), add(s2), add(s3), add(s4);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5) : Symbol(name, bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5, const Symbol& s6) : Symbol(name, bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5), add(s6);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5, const Symbol& s6, const Symbol& s7) : Symbol(name, bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5), add(s6), add(s7);
    }
    Bundling(String name, const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5, const Symbol& s6, const Symbol& s7, const Symbol& s8) : Symbol(name, bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5), add(s6), add(s7), add(s8);
    }
    Bundling(const Symbol& s1) : Symbol("", bundling) {
      add(s1);
    }
    Bundling(const Symbol& s1, const Symbol& s2) : Symbol("", bundling) {
      add(s1), add(s2);
    }
    Bundling(const Symbol& s1, const Symbol& s2, const Symbol& s3) : Symbol("", bundling) {
      add(s1), add(s2), add(s3);
    }
    Bundling(const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4) : Symbol("", bundling) {
      add(s1), add(s2), add(s3), add(s4);
    }
    Bundling(const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5) : Symbol("", bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5);
    }
    Bundling(const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5, const Symbol& s6) : Symbol("", bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5), add(s6);
    }
    Bundling(const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5, const Symbol& s6, const Symbol& s7) : Symbol("", bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5), add(s6), add(s7);
    }
    Bundling(const Symbol& s1, const Symbol& s2, const Symbol& s3, const Symbol& s4, const Symbol& s5, const Symbol& s6, const Symbol& s7, const Symbol& s8) : Symbol("", bundling) {
      add(s1), add(s2), add(s3), add(s4), add(s5), add(s6), add(s7), add(s8);
    }
    virtual ~Bundling();
    virtual std::string asString() const;
    virtual bool equals(const Symbol& symbol, char what = 'i') const;

    /**
     * @function add
     * @memberof Bundling
     * @instance
     * @description Adds a new symbol to the container.
     * @param {Symbol} symbol The symbol to add.
     * @return {Bundling} A reference to this bundling, allowing to chain add methods:
     * ```
     * macrovsa::Bundling bundling;
     * bundling.add(symbol_1).add(symbol_2). ../..
     * ```
     */
    Bundling& add(const Symbol& symbol);

    /**
     * @function erase
     * @memberof Bundling
     * @instance
     * @description Erases a symbol in the container.
     * @param {Symbol} [symbol ]The symbol to erase.
     *   - Without key argument erases all keys.
    * @return {AssociativeMap} A reference to this associative map, allowing to chain add methods.
     */
    Bundling& erase(const Symbol& symbol);
    Bundling& erase();

       /**
     * @function getSorted
     * @memberof Bundling
     * @instance
     * @description Returns the bundling symbols as a decreasing tau valuesorted associative map.
      * @return An AssociativeMap reference.
     */
    const AssociativeMap& getSorted() const;
  private:
    mutable AssociativeMap* sortedmap = NULL;
  public:
 
    /**
     * @function get
     * @memberof Bundling
     * @instance
     * @description Defines an iterator over the bundling symbols, used in a construct of the form:
     * ```
     *  for(auto it = bundling.Bundling::get().cbegin(); it != bundling.Bundling::get().cend(); it++) {
     *    // unsigned int symbolID = it->first;
     *    const Symbol& symbol = it->second;
     *    ../..
     *  }
     * ```
     * - The `bundling.Bundling::get().size()` construct returns the number of symbols in this bundling.
     * - The `bundling.Bundling::edit()` construct returns a modifiable `std::map <unsigned int, Symbol&>&` allowing to edit the underlying structure.
     * @return A `const std::map < unsigned int, Symbol& >&` reference, with symbols indexed by their indexes, for bundling iteration.
     */
    const std::map < unsigned int, Symbol& >& get() const {
      return values;
    }
    std::map < unsigned int, Symbol & > &edit() {
      vector = NULL;
      return values;
    }  
  };
}

#endif
