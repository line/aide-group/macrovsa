#include "AssociativeMap.hpp"
#include "Binding.hpp"
#include "algo.hpp"

namespace macrovsa {
  AssociativeMap& AssociativeMap::add(const Symbol& key, const Symbol& value)
  {
    Binding binding(key, value);
    Bundling::add(binding);
    return *this;
  }
  AssociativeMap& AssociativeMap::erase(const Symbol& key)
  {
    Binding binding(key, get (key));
    Bundling::erase(binding);
    return *this;
  }
  AssociativeMap& AssociativeMap::erase()
  {
    Bundling::erase();
    return *this;
  }
  const Symbol& AssociativeMap::get(const Symbol& key) const
  {
    Binding result(key, *this, false);
    return algo::reduce(result, symbols);
  }
  std::string AssociativeMap::asString() const
  {
    std::string result = wjson::quote(getName()) + ": { ";
    for(auto it = Bundling::get().cbegin(); it != Bundling::get().cend(); it++) {
      const Binding& binding = dynamic_cast < const Binding& > (it->second);
      result += "\n  '" + binding.y().asString() + "': '" + binding.x().asString() + "'";
    }
    return result + "}" + asStringTail() + "\n";
  }
}
