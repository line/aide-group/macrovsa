#ifndef __macrovsa_Array__
#define __macrovsa_Array__

#include "AssociativeMap.hpp"
#include "Number.hpp"

namespace macrovsa {
  /**
   * @class Array
   * @description Implements a macroscopic ersatz of a VSA indexed list.
   * - An indexed list is created adding new index/value pairs.
   * - It simply corresponds to an [AssociativeMap](./AssociativeMap.html) with [Number](./Number.html) as key.
   * - It is implemented via [Bundling](./Bundling.html) and [Binding](./Binding.html), without using a C++ `std::vector`.
   * @extends AssociativeMap
   */
  class Array: public AssociativeMap {
public:

    /**
     * @function add
     * @memberof Array
     * @instance
     * @description Adds a new symbol pair to the container.
     * - Adding twice the same index/value pair corresponds to adding their belief level `tau`.
     * @param {uint} index The symbol index to add or modify.
     * @param {Symbol} value The symbol value to add.
     * @return {Array} A reference to this associative map, allowing to chain add methods.
     */
    Array& add(unsigned int index, const Symbol& value)
    {
      return (Array&) AssociativeMap::add(Number::getInt(index), value);
    }
    /**
     * @function get
     * @memberof Array
     * @instance
     * @description Returns an approximate value of the associative map by unbinding.
     * @param {uint} index The symbol index.
     * @return {Symbol} value A reference to the stored value, available until program end.
     */
    const Symbol& get(unsigned int index) const
    {
      return AssociativeMap::get(Number::getInt(index));
    }
  };
}

#endif
