#include "algo.hpp"
#include "Bundling.hpp"
#include "Binding.hpp"
#include <vector>

namespace macrovsa {
  const Symbol& algo::reduce(const Symbol& symbol)
  {
    static Symbol::Symbols symbols;
    return reduce(symbol, symbols);
  }
  const Symbol& algo::reduce(const Symbol& symbol, Symbol::Symbols& symbols)
  {
    const Symbol& result = algo::reduce_symbol(symbol, symbols);
    return result;
  }
  const Symbol& algo::reduce_symbol(const Symbol& symbol, Symbol::Symbols& symbols)
  {
    switch(symbol.data.symbolType) {
    case Symbol::bundling:
    {
      const Bundling& s_ = dynamic_cast < const Bundling& > (symbol);
      // Expands the reduction through a bundling arguments
      Bundling *result = new Bundling();
      for(auto it = s_.get().cbegin(); it != s_.get().cend(); it++) {
        result->add(reduce(it->second, symbols));
      }
      return reduce_bundling(result, symbols);
    }
    case Symbol::binding:
    {
      const Binding& s_ = dynamic_cast < const Binding& > (symbol);
      if(s_.y().data.symbolType == Symbol::bundling) {
        // Expands binding on the y bundling argument
        const Bundling& y_ = dynamic_cast < const Bundling& > (s_.y());
        Bundling *result = new Bundling();
        for(auto it = y_.get().cbegin(); it != y_.get().cend(); it++) {
          Binding b_it(it->second, s_.x(), s_.b(), s_.normalized());
          result->add(reduce(b_it, symbols));
        }
        return reduce_bundling(result, symbols);
      } else {
        const Symbol& x_ = reduce(s_.x(), symbols);
        switch(x_.data.symbolType) {
        case Symbol::bundling:
        {
          // Expands binding on the x bundling argument
          const Bundling& x__ = dynamic_cast < const Bundling& > (x_);
          Bundling *result = new Bundling();
          for(auto it = x__.get().cbegin(); it != x__.get().cend(); it++) {
            Binding b_it(s_.y(), it->second, s_.b(), s_.normalized());
            result->add(reduce(b_it, symbols));
          }
          return reduce_bundling(result, symbols);
        }
        case Symbol::binding:
        {
          // Reduces dual binding unbinding operation
          const Binding& x__ = dynamic_cast < const Binding& > (x_);
          if(s_.y().equals(x__.y()) && s_.b() != x__.b()) {
            Symbol& result = Symbol::clone(reduce(x__.x(), symbols), symbols);
            result.belief.tau *= s_.belief.tau * x__.belief.tau;
            result.belief.sigma += s_.belief.sigma + x__.belief.sigma + algo::sigma_0;
            return result;
          }
        }
        default: // Symbol::atomic:
          return symbol;
        }
      }
    }
    default:
      return symbol;
    }
  }
  const Symbol& algo::reduce_bundling(Bundling *bundling, Symbol::Symbols& symbols)
  {
    // Empty bundling, returns the ``empty´´ symbol
    if(bundling->get().size() == 0) {
      Symbol *result = new Symbol("", bundling->belief.tau, bundling->belief.sigma);
      symbols.add(result);
      return *result;
    }
    // Bundling singleton, returns the unique symbol
    if(bundling->get().size() == 1) {
      return bundling->get().cbegin()->second;
    }
    symbols.add(bundling);
    return *bundling;
  }
  Belief algo::sim(const Symbol& s1, const Symbol& s2, bool reduce)
  {
    if(reduce) {
      return sim(algo::reduce(s1), algo::reduce(s2), false);
    }
    if(s2.data.symbolType == Symbol::bundling) {
      return sim(s2, s1, reduce);
    }
    if(s1.data.symbolType == Symbol::bundling) {
      const Bundling& s1_ = dynamic_cast < const Bundling& > (s1);
      Belief result(0, 0);
      for(auto it = s1_.get().cbegin(); it != s1_.get().cend(); it++) {
        Belief result_it = sim(it->second, s2, reduce);
        result.tau += result_it.tau;
        result.sigma += result_it.sigma;
      }
      return result;
    }
    Belief result(s1.data.id == s2.data.id ? s1.belief.tau *s2.belief.tau : 0, fabs (s1.belief.tau *s2.belief.tau) *sigma_0 + fabs (s2.belief.tau) *s1.belief.sigma + fabs (s1.belief.tau) *s2.belief.sigma);
    return result;
  }
  double algo::msim(const Symbol& s1, const Symbol& s2)
  {
    const double *v1 = s1.getVector(), *v2 = s2.getVector();
    double r = 0;
    for(unsigned int i = 0; i < Symbol::getDimension(); i++) {
      r += v1[i] * v2[i];
    }
    return r;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2)
  {
    Belief belief(fmax (0, fmin(a1.tau, a2.tau)), a1.sigma + a2.sigma);
    return belief;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2, const Belief& a3)
  {
    Belief belief(fmax (0, fmin(a1.tau, fmin(a2.tau, a3.tau))), a1.sigma + a2.sigma + a3.sigma);
    return belief;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2, const Belief& a3, const Belief& a4)
  {
    Belief belief(fmax (0, fmin(a1.tau, fmin(a2.tau, fmin(a3.tau, a4.tau)))), a1.sigma + a2.sigma + a3.sigma + a4.sigma);
    return belief;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2, const Belief& a3, const Belief& a4, const Belief& a5)
  {
    Belief belief(fmax (0, fmin(a1.tau, fmin(a2.tau, fmin(a3.tau, fmin(a4.tau, a5.tau))))), a1.sigma + a2.sigma + a3.sigma + a4.sigma + a5.sigma);
    return belief;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2, const Belief& a3, const Belief& a4, const Belief& a5, const Belief& a6)
  {
    Belief belief(fmax (0, fmin(a1.tau, fmin(a2.tau, fmin(a3.tau, fmin(a4.tau, fmin(a5.tau, a6.tau)))))), a1.sigma + a2.sigma + a3.sigma + a4.sigma + a5.sigma + a6.sigma);
    return belief;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2, const Belief& a3, const Belief& a4, const Belief& a5, const Belief& a6, const Belief& a7)
  {
    Belief belief(fmax (0, fmin(a1.tau, fmin(a2.tau, fmin(a3.tau, fmin(a4.tau, fmin(a5.tau, fmin(a6.tau, a7.tau))))))), a1.sigma + a2.sigma + a3.sigma + a4.sigma + a5.sigma + a6.sigma + a7.sigma);
    return belief;
  }
  Belief algo::conj(const Belief& a1, const Belief& a2, const Belief& a3, const Belief& a4, const Belief& a5, const Belief& a6, const Belief& a7, const Belief& a8)
  {
    Belief belief(fmax (0, fmin(a1.tau, fmin(a2.tau, fmin(a3.tau, fmin(a4.tau, fmin(a5.tau, fmin(a6.tau, fmin(a7.tau, a8.tau)))))))), a1.sigma + a2.sigma + a3.sigma + a4.sigma + a5.sigma + a6.sigma + a7.sigma + a8.sigma);
    return belief;
  }
  double algo::sigma_0 = 0, algo::sigma_04 = 0;
}
