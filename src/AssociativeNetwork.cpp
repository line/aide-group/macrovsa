#include "AssociativeNetwork.hpp"
#include "Binding.hpp"
#include "algo.hpp"

namespace macrovsa {
  const Symbol& AssociativeNetwork::get(const Symbol& key) const
  {
    Bundling *result = new Bundling(getName() + "::" + key.getName());
    for(auto it = Bundling::get().cbegin(); it != Bundling::get().cend(); it++) {
      const Binding& binding = dynamic_cast < const Binding& > (it->second);
      Belief s = algo::sim(key, binding.y());
      if(s.tau > 2 * (algo::sigma_0 + s.sigma)) {
        result->add(clone(binding.x(), s.tau, s.sigma));
      }
    }
    symbols.add(result);
    return *result;
  }
}
