#include "Rule.hpp"
#include "Binding.hpp"
#include "std.hpp"
#include "Rules.hpp"

namespace macrovsa {
  Rule::Rule(String name, unsigned int arity) : name(name), arity(arity)
  {
    aidesys::alert(!(1 <= arity & arity <= 3), "illegal-argument", "in macrovsa::Rule::Rule incorrect arity, %d should be in {1, 3}", arity);
  }
  Belief Rule::getTau(const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1)
  {
    aidesys::alert(true, (arity == 1 ? "illegal-state" : "illegal-argument"), (arity == 1 ? "in macrovsa::Rule::tau the tau() function of arity 1 has not been implemented" : "in macrovsa::Rule::tau the rule is not of arity 1 but %d"), arity);
    Belief belief;
    return belief;
  }
  Belief Rule::getTau(const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2)
  {
    aidesys::alert(true, (arity == 2 ? "illegal-state" : "illegal-argument"), (arity == 2 ? "in macrovsa::Rule::tau the tau() function of arity 2 has not been implemented" : "in macrovsa::Rule::tau  the rule is not of arity 2 but %d"), arity);
    Belief belief;
    return belief;
  }
  Belief Rule::getTau(const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2, const Symbol& subject_3, const Symbol& predicate_3, const Symbol& object_3)
  {
    aidesys::alert(true, (arity == 3 ? "illegal-state" : "illegal-argument"), (arity == 3 ? "in macrovsa::Rule::tau the tau() function of arity 3 has not been implemented" : "in macrovsa::Rule::tau  the rule is not of arity 3 but %d"), arity);
    Belief belief;
    return belief;
  }
  bool Rule::isValid(const Belief& belief)
  {
    return belief.tau > 2 * belief.sigma;
  }
  void Rule::setOutput(RelationalMap& output)
  {
    aidesys::alert(true, "illegal-state", "in macrovsa::Rule::out the output value mechanism of the rule has not been implemented");
  }
  void Rule::apply(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0)
  {
    switch(arity) {
    case 1:
      apply_1(output, input, subject_0, predicate_0, object_0);
      break;
    case 2:
      apply_2(output, input, subject_0, predicate_0, object_0);
      break;
    case 3:
      apply_3(output, input, subject_0, predicate_0, object_0);
      break;
    }
  }
  void Rule::apply_1(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0)
  {
    applyOnce(output, subject_0, predicate_0, object_0);
  }
  void Rule::apply_2(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0)
  {
    for(auto it = input.Bundling::get().cbegin(); it != input.Bundling::get().cend(); it++) {
      const Binding& binding_spo = dynamic_cast < const Binding& > (it->second);
      const Binding& binding_po = dynamic_cast < const Binding& > (binding_spo.x());
      const Symbol& subject_1 = binding_spo.y(), & predicate_1 = binding_po.y(), & object_1 = binding_po.x();
      applyOnce(output, subject_0, predicate_0, object_0, subject_1, predicate_1, object_1);
      applyOnce(output, subject_1, predicate_1, object_1, subject_0, predicate_0, object_0);
    }
  }
  void Rule::apply_3(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0)
  {
    for(auto it_1 = input.Bundling::get().cbegin(); it_1 != input.Bundling::get().cend(); it_1++) {
      const Binding& binding_spo_1 = dynamic_cast < const Binding& > (it_1->second);
      const Binding& binding_po_1 = dynamic_cast < const Binding& > (binding_spo_1.x());
      const Symbol& subject_1 = binding_spo_1.y(), & predicate_1 = binding_po_1.y(), & object_1 = binding_po_1.x();
      for(auto it_2 = input.Bundling::get().cbegin(); it_2 != input.Bundling::get().cend(); it_2++) {
        const Binding& binding_spo_2 = dynamic_cast < const Binding& > (it_2->second);
        const Binding& binding_po_2 = dynamic_cast < const Binding& > (binding_spo_2.x());
        const Symbol& subject_2 = binding_spo_2.y(), & predicate_2 = binding_po_2.y(), & object_2 = binding_po_2.x();
        applyOnce(output, subject_0, predicate_0, object_0, subject_1, predicate_1, object_1, subject_2, predicate_2, object_2);
        applyOnce(output, subject_0, predicate_0, object_0, subject_2, predicate_2, object_2, subject_1, predicate_1, object_1);
        applyOnce(output, subject_1, predicate_1, object_1, subject_0, predicate_0, object_0, subject_2, predicate_2, object_2);
        applyOnce(output, subject_1, predicate_1, object_1, subject_2, predicate_2, object_2, subject_0, predicate_0, object_0);
        applyOnce(output, subject_2, predicate_2, object_2, subject_1, predicate_1, object_1, subject_0, predicate_0, object_0);
        applyOnce(output, subject_2, predicate_2, object_2, subject_0, predicate_0, object_0, subject_1, predicate_1, object_1);
      }
    }
  }
  void Rule::applyOnce(RelationalMap& output, const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1)
  {
    aidesys::alert(arity != 1, "illegal-argument", "in macrovsa::Rule::apply the rule is not of arity 1 but %d", arity);
    this->subject_1 = &subject_1, this->predicate_1 = &predicate_1, this->object_1 = &object_1;
    tau = getTau(subject_1, predicate_1, object_1);
    dump_lhs = RelationalMap::asString(subject_1, predicate_1, object_1) + " =" + name + "_<" + tau.asString() + "> => ";
    applyOutput(output);
  }
  void Rule::applyOnce(RelationalMap& output, const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2)
  {
    aidesys::alert(arity != 2, "illegal-argument", "in macrovsa::Rule::tau the rule is not of arity 2 but %d", arity);
    this->subject_1 = &subject_1, this->predicate_1 = &predicate_1, this->object_1 = &object_1;
    this->subject_2 = &subject_2, this->predicate_2 = &predicate_2, this->object_2 = &object_2;
    tau = getTau(subject_1, predicate_1, object_1, subject_2, predicate_2, object_2);
    dump_lhs = RelationalMap::asString(subject_1, predicate_1, object_1) + " ^ " + RelationalMap::asString(subject_2, predicate_2, object_2) + " =" + name + "_<" + tau.asString() + "> => ";
    applyOutput(output);
  }
  void Rule::applyOnce(RelationalMap& output, const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2, const Symbol& subject_3, const Symbol& predicate_3, const Symbol& object_3)
  {
    aidesys::alert(arity != 3, "illegal-argument", "in macrovsa::Rule::tau the rule is not of arity 3 but %d", arity);
    this->subject_1 = &subject_1, this->predicate_1 = &predicate_1, this->object_1 = &object_1;
    this->subject_2 = &subject_2, this->predicate_2 = &predicate_2, this->object_2 = &object_2;
    this->subject_3 = &subject_3, this->predicate_3 = &predicate_3, this->object_3 = &object_3;
    tau = getTau(subject_1, predicate_1, object_1, subject_2, predicate_2, object_2, subject_3, predicate_3, object_3);
    dump_lhs = RelationalMap::asString(subject_1, predicate_1, object_1) + " ^ " + RelationalMap::asString(subject_2, predicate_2, object_2) + " ^ " + RelationalMap::asString(subject_3, predicate_3, object_3) + " =" + name + "_<" + tau.asString() + ">=>";
    applyOutput(output);
  }
  void Rule::applyOutput(RelationalMap& output)
  {
    bool valid = isValid(tau);
    // unsigned int i0 = output.Bundling::get().size();
    if(valid) {
      setOutput(output);
    }
    // Dumps the inference trace
    {
      char what = valid ? 'a' : 'u';
      dump(dump_lhs, what);

      /*
       *  for(unsigned int i = i0; i < output.Bundling::get().size(); i++) {
       *  const Binding& binding_spo = dynamic_cast < const Binding& > (output.Bundling::get().at(i));
       *  const Binding& binding_po = dynamic_cast < const Binding& > (binding_spo.x());
       *  const Symbol& subject_0 = binding_spo.y(), predicate_0 = binding_po.y(), object_0 = binding_po.x();
       *  dump(" " + RelationalMap::asString(subject_0, predicate_0, object_0), what);
       *  }
       */
      dump("\n", what);
    }
  }
  void Rule::dump(String string, char what)
  {
    if(rules != NULL) {
      rules->dump(string, what);
    }
  }
}
