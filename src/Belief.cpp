#include "Belief.hpp"
#include "std.hpp"

namespace macrovsa {
  std::string Belief::asString() const
  {
    return aidesys::echo("%.2g±%.2g", tau, sigma);
  }
}
