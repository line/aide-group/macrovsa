#include "Symbol.hpp"
#include "Bundling.hpp"
#include "Binding.hpp"
#include "algo.hpp"
#include "random.hpp"
#include "regex.hpp"
#include "Value.hpp"
#include <string.h>
#include <typeinfo>

namespace macrovsa {
  //
  // Global values and static methods
  //
  unsigned int Symbol::dimension = 0, Symbol::dimension_sqrt = 0;
  std::map < std::string, Symbol::SymbolData > Symbol::datas;
  Symbol::SymbolData& Symbol::getData(String name_, SymbolType symbolType)
  {
    static unsigned int ids = 0;
    std::string name = name_ == "" ? aidesys::echo("#%d", ids) : name_;
    if(datas.find(name) == datas.cend()) {
      datas[name].name = name;
      datas.at(name).id = ids++;
      datas.at(name).symbolType = symbolType;
    } else {
      aidesys::alert(datas.at(name).symbolType != symbolType, "illegal-state", "in macrovsa::Symbol::Symbol incoherence in symbol type was '%s' and now '%s'", SymbolData::fromJSONTypeName(datas.at(name).symbolType), SymbolData::fromJSONTypeName(symbolType));
    }
    datas.at(name).count++;
    return datas.at(name);
  }
  const char *Symbol::SymbolData::fromJSONTypeName(SymbolType symbolType)
  {
    return symbolType == bundling ? "bundling" : symbolType == binding ? "binding" : "atomic";
  }
  void Symbol::setDimension(unsigned int d)
  {
    dimension = d, dimension_sqrt = (int) sqrt(d);
    aidesys::alert(dimension_sqrt * dimension_sqrt != dimension, "illegal-argument", "in macrovsa::Symbol::setDimension the given dimension %d != %d^2 is not a square of an integer", dimension, dimension_sqrt);
    // Uses a rule of thum
    double g = 1024 * (d < 100 ? 1 : (6 * log(d) / log(10) - 11));
    algo::sigma_0 = pow(g * d, -1), algo::sigma_04 = pow(algo::sigma_0, 0.25);
    for(auto it = datas.begin(); it != datas.end(); it++) {
      if(it->second.vector != NULL) {
        delete[] it->second.vector;
        it->second.vector = NULL;
      }
    }
  }
  bool Symbol::initDimension()
  {
    setDimension();
    return true;
  }
  bool Symbol::initDimensionDone = Symbol::initDimension();
  //
  // Symbol instance methods
  //
  Symbol::Symbol(String name, double tau, double sigma, SymbolType symbolType) : data(getData(name, symbolType)), belief(tau, sigma)
  {}
  Symbol::~Symbol()
  {
    data.count--;
    if(data.count == 0) {
      delete[] data.vector;
      data.vector = NULL;
    }
    delete[] vector;
  }
  const double *Symbol::getVector() const
  {
    if(data.vector == NULL) {
      data.vector = new double[dimension];
      const_cast < Symbol * > (this)->setVector(data.vector);
      if(belief.tau != 1) {
        delete[] vector;
        vector = new double[dimension];
        for(unsigned int i = 0; i < dimension; i++) {
          vector[i] = belief.tau * data.vector[i];
        }
      } else {
        delete[] vector;
      }
    }
    return belief.tau != 1 ? vector : data.vector;
  }
  void Symbol::setVector(double *vector)
  {
    double m = 0;
    for(unsigned int i = 0; i < dimension; i++) {
      vector[i] = aidesys::random('n'), m += vector[i] * vector[i];
    }
    if(m > 0) {
      m = sqrt(m);
      for(unsigned int i = 0; i < dimension; i++) {
        vector[i] /= m;
      }
    }
  }
  std::string Symbol::asString() const
  {
    aidesys::alert(data.symbolType == bundling || data.symbolType == binding, "illegal-state", "in macrovsa::Symbol::asString() '%s' stringify as a symbol while with a type %s", data.name.c_str(), SymbolData::fromJSONTypeName(data.symbolType));
    return data.name + asStringTail();
  }
  std::string Symbol::asStringTail() const
  {
    return belief.tau == 1 && belief.sigma == 0 ? "" : "_<" + belief.asString() + ">";
  }
  bool Symbol::equals(const Symbol& symbol, char what) const
  {
    return data.id == symbol.data.id &&
           (what == 'c' ||
            (fabs(belief.tau - symbol.belief.tau) <= 2 * (algo::sigma_0 + belief.sigma + symbol.belief.sigma)));
  }
  Symbol Symbol::nill("", 0, 0);
  // Symbols remamence management
  void Symbol::Symbols::add(Symbol *symbol)
  {
    remanent_values.insert(symbol);
  }
  Symbol::Symbols::~Symbols()
  {
    for(auto it = remanent_values.begin(); it != remanent_values.end(); it++) {
      delete(*it);
    }
  }
  Symbol& Symbol::clone(const Symbol& symbol, Symbols& symbols, double tau, double sigma)
  {
    Symbol *result;
    switch(symbol.data.symbolType) {
    case bundling:
      result = new Bundling(dynamic_cast < const Bundling& > (symbol), symbols);
      break;
    case binding:
      result = new Binding(dynamic_cast < const Binding& > (symbol), symbols);
      break;
    default: // atomic
      result = new Symbol(symbol, tau, sigma);
      break;
    }
    symbols.add(result);
    return *result;
  }
  Symbol& Symbol::clone(const Symbol& symbol, double tau, double sigma)
  {
    static Symbols symbols;
    return clone(symbol, symbols, tau, sigma);
  }
  Symbol& Symbol::clone(const Symbol& symbol, Symbols& symbols, const Belief& belief)
  {
    return clone(symbol, symbols, belief.tau, belief.sigma);
  }
  Symbol& Symbol::clone(const Symbol& symbol, const Belief& belief)
  {
    return clone(symbol, belief.tau, belief.sigma);
  }
  Symbol& Symbol::fromJSON(JSON symbol)
  {
    static Symbols symbols;
    if(symbol.isArray()) {
      Bundling b;
      for(unsigned int i = 0; i < symbol.length(); i++) {
        Symbol& s = fromJSON(symbol.at(i));
        b.add(s);
      }
      return clone(b, symbols);
    } else if(symbol.isRecord()) {
      if(symbol.isMember("x") && symbol.isMember("y")) {
        Binding b(fromJSON (symbol.at("y")), fromJSON (symbol.at("x")), !symbol.get("u", false));
        return clone(b, symbols);
      } else if(symbol.isMember("name")) {
        Symbol s(symbol.get("name", ""), symbol.get("tau", 1.0), symbol.get("sigma", 0.0));
        return clone(s, symbols);
      }
    }
    Symbol s(symbol.asString());
    return clone(s, symbols);
  }
  Symbol& Symbol::fromJSON(String symbol)
  {
    return fromJSON(wjson::string2json(symbol));
  }
  Symbol& Symbol::fromJSON(const char *symbol)
  {
    return fromJSON(wjson::string2json(symbol));
  }
  std::string Symbol::toJSON(const Symbol& symbol)
  {
    switch(symbol.data.symbolType) {
    case bundling:
    {
      const Bundling& s = dynamic_cast < const Bundling& > (symbol);
      std::string result = "[";
      for(auto it = s.get().cbegin(); it != s.get().cend(); it++) {
        result += " " + toJSON(it->second);
      }
      return result + " ]";
    }
    case binding:
    {
      const Binding& s = dynamic_cast < const Binding& > (symbol);
      return (std::string) "{" + (s.b() ? "b" : "u") + " y: " + toJSON(s.y()) + " x: " + toJSON(s.y()) + "}";
    }
    default: // atomic
      return aidesys::echo("{ name: " + symbol.getName() + " tau: %g sigma: %g}", symbol.belief.tau, symbol.belief.sigma);
    }
  }
  void Symbol::dump()
  {
    printf("{ Symbol data:\n");
    for(auto it = datas.begin(); it != datas.end(); it++) {
      printf("  { name: '%s' id: %d count: %d type: %s with_vector: %d }\n", it->first.c_str(), it->second.id, it->second.count, SymbolData::fromJSONTypeName(it->second.symbolType), it->second.vector != NULL);
    }
    printf("}\n");
  }
}
