#ifndef __macrovsa_Belief__
#define __macrovsa_Belief__

#include <string>

namespace macrovsa {
  /**
   * @class Belief
   * @description Implements a degree of belief value and its precision.
   * - A belief is non-negligible up to a confidence interval of `99%` if ``tau > 2 sigma`.
   * - A belief is taken as negligible if ``tau < sigma`, taking a numerical margin into account.
   * @param [tau=1] The degree of belief between -1 (false), 0 (unknown) and true (1).
   * @param [sigma=0] The standard-deviation of the level of noise of the given belief value.
   */
  class Belief {
public:
    Belief(double tau = 1, double sigma = 0) : tau(tau), sigma(sigma) {}
    Belief(const Belief& belief) : tau(belief.tau), sigma(belief.sigma) {}

    /**
     * @member {double} tau
     * @memberof Belief
     * @instance
     * @description The degree of belief between -1 (false), 0 (unknown) and true (1), 0 by default.
     */
    double tau = 1;

    /**
     * @member {double} sigma
     * @memberof Belief
     * @instance
     * @description The standard-deviation of the level of noise of the given belief value, 1 by default.
     * - The standard-deviation is bounded to 1, because such a random vector is ill-defined at such level of randomness.
     */
    double sigma = 0;

    /**
     * @function asString
     * @memberof Belief
     * @instance
     * @description Returns the value as a string.
     * @return {String} A string of the form `tau+/-sigma`.
     */
    virtual std::string asString() const;
  };
}
#endif
