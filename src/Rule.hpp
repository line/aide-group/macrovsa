#ifndef __macrovsa_Rule__
#define __macrovsa_Rule__

#include "Belief.hpp"
#include "Symbol.hpp"
#include "RelationalMap.hpp"

namespace macrovsa {
  class Rules;

  /**
   * @class Rule
   * @description Implements an abstract rule based mechanism, based on belief calculation.
   * - A rule is implemented by overloading the [`getTau()`](#.getTau) and [`getOutput()`](#.getOutput) functions.
   * - A rule is validated by the the [`isValid()`](#.isValid) function.
   * - A rule is used through the [`Rules`](Rules.html) mechanism, which provides examples of construction
   * - via [a macro](Rules.html) or
   * - as a [derived class](Rules.html#.add).
   * @param {uint} arity The rule arity, actually, the implementation considers rules of arity 1, 2 or 3.
   */
  class Rule {
    std::string name;
    unsigned int arity;
protected:
    const Symbol * subject_1 = NULL, *predicate_1 = NULL, *object_1 = NULL, *subject_2 = NULL, *predicate_2 = NULL, *object_2 = NULL, *subject_3 = NULL, *predicate_3 = NULL, *object_3 = NULL;
    Belief tau;
public:
    Rule(String name, uint arity);

    /**
     * @function getName
     * @memberof Rule
     * @instance
     * @description Returns the rule name.
     */
    String getName() const
    {
      return name;
    }
    /**
     * @function getArity
     * @memberof Rule
     * @instance
     * @description Returns the rule arity.
     */
    unsigned int getArity() const
    {
      return arity;
    }
    /**
     * @function getTau
     * @memberof Rule
     * @instance
     * @description Returns the tau value given triples.
     * - This method is
     *   - to be overloaded to implement the rule, depending on the arity;
     *   - not expected to be called directly, but though the apply function.
     * - This method is usually built using the [algo::sim()](./algo.html#.sim) and [algo::conj()](./algo.html#.conf) functions.
     * @param {Symbol} subject_1 The 1st triple subject.
     * @param {Symbol} predicate_1 The 1st triple predicate.
     * @param {Symbol} object_1 The 1st triple object.
     * @param {Symbol} [subject_2] The 2nd triple subject.
     * @param {Symbol} [predicate_2] The 2nd triple predicate.
     * @param {Symbol} [object_2] The 2nd triple object.
     * @param {Symbol} [subject_3] The 3rd triple subject.
     * @param {Symbol} [predicate_3] The 3rd triple predicate.
     * @param {Symbol} [object_3] The 3rd triple object.
     * @return {Belief} The rule evaluation of the belief.
     */
    virtual Belief getTau(const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1);
    virtual Belief getTau(const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2);
    virtual Belief getTau(const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2, const Symbol& subject_3, const Symbol& predicate_3, const Symbol& object_3);

    /**
     * @function isValid
     * @memberof Rule
     * @instance
     * @description Returns true if the level of belief is valid for this rule.
     * - This method is to be overloaded to implement a specific validity criterion.
     * - The default implementation returns `belief.tau > 2 * belief.sigma`.
     * @param {Belief} belief A `const Belief&` input value.
     *  @ @return {bool} True if valid, false otherwise.
     */
    virtual bool isValid(const Belief& belief);

    /**
     * @function setOutput
     * @memberof Rule
     * @instance
     * @description Returns the output the rule given the input triples.
     * - This method is
     *   - to be overloaded to implement the rule;
     *   - not expected to be called directly, but though the apply function.
     * - In order to calculate the output triples, this method access to the protected:
     *   - `tau` belief value, previously calculated;
     *   - the protected pointers to input values `*subject_i`, `*predicate_i`, `*object_i`, previously set.
     * A typical implementation writes:
     * ```
     * virtual void(RelationalMap& output) {
     *   output.add(subject_0, predicate_0, object_0); // where these arguments correspond to an output value
     * }
     * ```
     * @param {RelationalMap} output A `RelationalMap *` pointer to a relation map with the output triples, to be deleted after use.
     */
    virtual void setOutput(RelationalMap& output);
private:
    friend class Rules;
    // Applies the rule on a given triple against all an input map triples and sets the output.

    /*
     * @param {RelationalMap} output A relation map reference where to output the infered triples if the rule is valid.
     * @param {RelationalMap} input A relation map reference with triples for rule application with the given triple.
     * @param {Symbol} subject_0 The input triple subject.
     * @param {Symbol} predicate_0 The input triple predicate.
     * @param {Symbol} object_0 The input triple object.
     */
    void apply(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0);
private:
    // Applies the rule on a given triple against an input map and sets the output.
    void apply_1(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0);
    void apply_2(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0);
    void apply_3(RelationalMap& output, const RelationalMap& input, const Symbol& subject_0, const Symbol& predicate_0, const Symbol& object_0);
    // Applies once the rule on a given input and sets the output.
    // This method properly calls the [`getTau()`](#.getTau) and [`getOutput()`](#.getOutput) functions, using the [`isValid()`](#.isValid) function.
    // This method is used by a apply_i method.
    void applyOnce(RelationalMap& output, const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1);
    void applyOnce(RelationalMap& output, const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2);
    void applyOnce(RelationalMap& output, const Symbol& subject_1, const Symbol& predicate_1, const Symbol& object_1, const Symbol& subject_2, const Symbol& predicate_2, const Symbol& object_2, const Symbol& subject_3, const Symbol& predicate_3, const Symbol& object_3);
    void applyOutput(RelationalMap& output);
    // This section is used to trace the mechanism
    void dump(String string, char what);
    std::string dump_lhs;
    Rules *rules = NULL;
  };
}
#endif
