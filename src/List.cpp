#include "List.hpp"
#include "Binding.hpp"
#include "algo.hpp"
namespace macrovsa {
  List& List::add(const Symbol& current, const Symbol& value)
  {
    const Symbol& next = getNext(current);
    AssociativeMap::erase(current);
    AssociativeMap::add(current, value);
    AssociativeMap::add(value, next);
    return *this;
  }
  List& List::erase(const Symbol& current, const Symbol& value)
  {
    const Symbol& previous = getPrevious(current), & next = getNext(current);
    AssociativeMap::erase(previous);
    AssociativeMap::erase(current);
    AssociativeMap::add(previous, next);
    return *this;
  }
  const Symbol& List::getNext(const Symbol& current) const
  {
    return get(current);
  }
  const Symbol& List::getPrevious(const Symbol& current) const
  {
    for(auto it = Bundling::get().cbegin(); it != Bundling::get().cend(); it++) {
      const Binding& binding = dynamic_cast < const Binding& > (it->second);
      if(binding.x().equals(current)) {
        return binding.y();
      }
    }
    return nill;
  }
}
