#ifndef __macrovsa_AssociativeNetwork__
#define __macrovsa_AssociativeNetwork__

#include "Symbol.hpp"
#include "AssociativeMap.hpp"
#include <map>

namespace macrovsa {
  /**
   * @class AssociativeNetwork
   * @description Implements a macroscopic ersatz of a VSA associative network.
   * - An associative network is created adding new key/value pairs of symbol.
   * - It is implemented via a matrix of the form `>_i value_i key_i^T`
   * @param {string} [name] An optional name. By default, a `#index` unique name is generated.
   * @extends AssociativeMap
   */
  class AssociativeNetwork: public AssociativeMap {
public:
    AssociativeNetwork() {}
    AssociativeNetwork(String name) : AssociativeMap(name) {}

    /**
     * @function add
     * @memberof AssociativeNetwork
     * @instance
     * @description Adds a new symbol pair to the container.
     * - Adding twice the same key/value pair corresponds to adding their belief level `tau`.
     * @param {Symbol} key The symbol key to add or modify.
     * @param {Symbol} value The symbol value to add.
     * @return {AssociativeNetwork} A reference to this associative map, allowing to chain add methods.
     */

    /**
     * @function erase
     * @memberof AssociativeNetwork
     * @instance
     * @description Erases a symbol in the container.
     * @param {Symbol} key The symbol key to add or modify.
     * @return {AssociativeNetwork} A reference to this associative map, allowing to chain add methods.
     */

    /**
     * @function get
     * @memberof AssociativeNetwork
     * @instance
     * @description Returns an approximate value of the associative map by unbinding.
     * - For a map with `>_i ` value_i key_i returns
     *    - `(key_i^T key) value_i + noise`, i.e,  `value_o + noise` if the if `key = key_o` and is the only key.
     * @param {Symbol} key The symbol key.
     * @return {Symbol} value A reference to the stored value `value_i`, available until program end.
     */
    const Symbol& get(const Symbol& key) const;

    /**
     * @function get
     * @memberof AssociativeNetwork
     * @instance
     * @description Defines an iterator over the associative map symbols, used in a construct of the form:
     * ```
     *  for(auto it = associativeNetwork.Bundling::get().cbegin(); it != associativeNetwork.Bundling::get().cend(); it++) {
     *    const Binding& binding = dynamic_cast < const Binding& > (it->second);
     *    const Symbol& key = binding.y(), value = binding.x();
     *    ../..
     *  }
     * ```
     * @return A `const std::map < Symbol* >&` reference for associative map iteration.
     */

    /**
     * @function asString
     * @memberof AssociativeNetwork
     * @instance
     * @description Returns the value as a string.
     * @return {String} A string of the form `{ key: value ...}_<belief>`, omitting the belief if `tau=1, sigma=0`.
     */
  };
}

#endif
