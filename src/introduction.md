@aideAPI

### This only an alpha version, better contact us to discuss, what it is and how to use it :)

The usual Vector Symbolic Architecture are implemented at microscopic using, e.g., the Neural Engineering Framework allowing a microscopic simulation of the neuronal processes, at the spiking neural network level. At a higher scale, when implemented using Semantic Pointer Architecture, based on linear algebra and permutation operations, thus at a mesoscopic scale, it allows to perform the same operations, but without explicitizing the neural state value and evolution.

A step further, at a higher macroscopic scale, we could directly consider the previous operations predicting the result of the different algebraic operations without explicitly calculating on vector components. This could be called an ``algorithmic ersatz'', and this is what is implemented here.

Please refer to the draft in submission regarding:
- [Algorithmic ersatz for VSA](https://www.overleaf.com/read/pmjznjqsctym)
  - with the related [numerical results](./macrovsa_experiments.pdf)
  - and related [inference mechanism trace](small_pizza_experiments.out.txt).
- [Biologically plausible reasoning embedded in neuronal computation](https://www.overleaf.com/read/kjwbnzkpvdxq#09e17b)
  - with the related [inference mechanism trace](pizza_experiments.out.txt).




