#ifndef __macrovsa_RelationalMap__
#define __macrovsa_RelationalMap__

#include "Symbol.hpp"
#include "Bundling.hpp"

namespace macrovsa {
  /**
   * @class RelationalMap
   * @description Implements a macroscopic ersatz of a VSA relational map.
   * - An relational map is created adding new symbols triples.
   * - It is implemented via [Bundling](./Bundling.html) and double [Binding](./Binding.html), without using a C++ map.
   * @extends Bundling
   */
  class RelationalMap: public Bundling {
public:
    RelationalMap() {}
    RelationalMap(String name) : Bundling(name) {}

    /**
     * @function add
     * @memberof RelationalMap
     * @instance
     * @description Adds a new symbolic triple to the container.
     * - For convinience the symbol can be given by its name, considering that `tau = 1, sigma = 0`.
     * @param {Symbol|String} subject The triple subject to add.
     * @param {Belief|double} [belief] An optional belief, or `tau` value, applied on the predicate.
     * @param {Symbol|String} predicate The triple predicate to add.
     * @param {Symbol|String} object The triple object to add.
     * @return {RelationalMap} A reference to this relational map, allowing to chain add methods.
     */
    RelationalMap& add(const Symbol& subject, const Symbol& predicate, const Symbol& object);
    RelationalMap& add(String subject, const Symbol& predicate, const Symbol& object)
    {
      const Symbol s(subject);
      return add(s, predicate, object);
    }
    RelationalMap& add(const Symbol& subject, String predicate, const Symbol& object)
    {
      const Symbol p(predicate);
      return add(subject, p, object);
    }
    RelationalMap& add(const Symbol& subject, const Symbol& predicate, String object)
    {
      const Symbol o(object);
      return add(subject, predicate, o);
    }
    RelationalMap& add(String subject, String predicate, const Symbol& object)
    {
      const Symbol s(subject), p(predicate);
      return add(s, p, object);
    }
    RelationalMap& add(String subject, const Symbol& predicate, String object)
    {
      Symbol s(subject), o(object);
      return add(s, predicate, o);
    }
    RelationalMap& add(const Symbol& subject, String predicate, String object)
    {
      const Symbol p(predicate), o(object);
      return add(subject, p, o);
    }
    RelationalMap& add(String subject, String predicate, String object)
    {
      const Symbol s(subject), p(predicate), o(object);
      return add(s, p, o);
    }
    RelationalMap& add(const Symbol& subject, const Belief& belief, const Symbol& predicate, const Symbol& object)
    {
      return add(subject, clone(predicate, symbols, belief), object);
    }
    RelationalMap& add(String subject, const Belief& belief, const Symbol& predicate, const Symbol& object)
    {
      const Symbol s(subject);
      return add(s, clone(predicate, symbols, belief), object);
    }
    RelationalMap& add(const Symbol& subject, const Belief& belief, String predicate, const Symbol& object)
    {
      const Symbol p(predicate);
      return add(subject, clone(p, symbols, belief), object);
    }
    RelationalMap& add(const Symbol& subject, const Belief& belief, const Symbol& predicate, String object)
    {
      const Symbol o(object);
      return add(subject, clone(predicate, symbols, belief), o);
    }
    RelationalMap& add(String subject, const Belief& belief, String predicate, const Symbol& object)
    {
      Symbol s(subject), p(predicate);
      return add(s, clone(p, symbols, belief), object);
    }
    RelationalMap& add(String subject, const Belief& belief, const Symbol& predicate, String object)
    {
      Symbol s(subject), o(object);
      return add(s, clone(predicate, symbols, belief), o);
    }
    RelationalMap& add(const Symbol& subject, const Belief& belief, String predicate, String object)
    {
      Symbol p(predicate), o(object);
      return add(subject, clone(p, symbols, belief), o);
    }
    RelationalMap& add(String subject, const Belief& belief, String predicate, String object)
    {
      Symbol s(subject), p(predicate), o(object);
      return add(s, clone(p, symbols, belief), o);
    }
    /**
     * @function add
     * @memberof RelationalMap
     * @instance
     * @description Adds all triple of a relation map in this one.
     * @param {RelationMap} map The triples to append to this relation map
     * @return {RelationalMap} A reference to this relational map, allowing to chain add methods.
     */
    RelationalMap& add(const RelationalMap& map);

    /**
     * @function add
     * @memberof RelationalMap
     * @instance
     * @description Adds all triple read from a text file with N-Triples syntax.
     * @param {String} filename The filename [N-Triples](https://en.wikipedia.org/wiki/N-Triples) syntax, with the triples to append to this relation map
     * @return {RelationalMap} A reference to this relational map, allowing to chain add methods.
     */
    RelationalMap& add(String filename);

    /**
     * @function get
     * @memberof RelationalMap
     * @instance
     * @description Returns an approximate value of the relational map by unbinding.
     * @param {Symbol} name The symbol name.
     * @return {Symbol} value A pointer to the stored value, to be deleted after use.
     */
    Symbol *get(const Symbol& name) const;

    /**
     * @function get
     * @memberof RelationalMap
     * @instance
     * @description Defines an iterator over the relational map symbols, used in a construct of the form:
     * ```
     *  for(auto it = relationalMap.Bundling::get().cbegin(); it != relationalMap.Bundling::get().cend(); it++) {
     *    const Binding& binding_spo = dynamic_cast <const Binding&> (it->second);
     *    const Binding& binding_po = dynamic_cast <const Binding&> (binding_spo.x());
     *    const Symbol& subject = binding_spo.y(), predicate = binding_po.y(), object = binding_po.x();
     *    ../..
     *  }
     * ```
     * @return A `const std::map <unsigned int, Symbol& >&` reference for relational map iteration.
     */

    /**
     * @function asString
     * @memberof RelationalMap
     * @instance
     * @description Returns the value as a string.
     * @return {String} A string of the form `{\n\t(subject_i predicate_i object_i)_<belief> ...\n}_<belief>`, omitting belief if `tau=1, sigma=0`.
     */
    virtual std::string asString() const;

    /**
     * @function asString
     * @memberof RelationalMap
     * @static
     * @description Returns the value of a triple as a string.
     * - For convinience the symbol can be given by its name, considering that `tau = 1, sigma = 0`.
     * @param {Symbol|String} subject The triple subject to add.
     * @param {Belief} [belief] An optional belief apply on the predicate.
     * @param {Symbol|String} predicate The triple predicate to add.
     * @param {Symbol|String} object The triple object to add.
     * @return {String} A string of the form `(subject_i predicate_i object_i)_<belief>`, omitting belief if `tau=1, sigma=0`.
     */
    static std::string asString(const Symbol& subject, const Symbol& predicate, const Symbol& object);
    static std::string asString(String subject, const Symbol& predicate, const Symbol& object)
    {
      Symbol s(subject);
      return asString(s, predicate, object);
    }
    static std::string asString(const Symbol& subject, String predicate, const Symbol& object)
    {
      Symbol p(predicate);
      return asString(subject, p, object);
    }
    static std::string asString(const Symbol& subject, const Symbol& predicate, String object)
    {
      Symbol o(object);
      return asString(subject, predicate, o);
    }
    static std::string asString(String subject, String predicate, const Symbol& object)
    {
      Symbol s(subject), p(predicate);
      return asString(s, p, object);
    }
    static std::string asString(String subject, const Symbol& predicate, String object)
    {
      Symbol s(subject), o(object);
      return asString(s, predicate, o);
    }
    static std::string asString(const Symbol& subject, String predicate, String object)
    {
      Symbol p(predicate), o(object);
      return asString(subject, p, o);
    }
    static std::string asString(String subject, String predicate, String object)
    {
      Symbol s(subject), p(predicate), o(object);
      return asString(s, p, o);
    }
    static std::string asString(const Symbol& subject, const Belief& belief, const Symbol& predicate, const Symbol& object)
    {
      Symbol p(predicate, belief);
      return asString(subject, p, object);
    }
    static std::string asString(String subject, const Belief& belief, const Symbol& predicate, const Symbol& object)
    {
      Symbol s(subject), p(predicate, belief);
      return asString(s, p, object);
    }
    static std::string asString(const Symbol& subject, const Belief& belief, String predicate, const Symbol& object)
    {
      Symbol p(predicate, belief);
      return asString(subject, p, object);
    }
    static std::string asString(const Symbol& subject, const Belief& belief, const Symbol& predicate, String object)
    {
      Symbol p(predicate, belief), o(object);
      return asString(subject, p, o);
    }
    static std::string asString(String subject, const Belief& belief, String predicate, const Symbol& object)
    {
      Symbol s(subject), p(predicate, belief);
      return asString(s, p, object);
    }
    static std::string asString(String subject, const Belief& belief, const Symbol& predicate, String object)
    {
      Symbol s(subject), p(predicate, belief), o(object);
      return asString(s, p, o);
    }
    static std::string asString(const Symbol& subject, const Belief& belief, String predicate, String object)
    {
      Symbol p(predicate, belief), o(object);
      return asString(subject, p, o);
    }
    static std::string asString(String subject, const Belief& belief, String predicate, String object)
    {
      Symbol s(subject), p(predicate, belief), o(object);
      return asString(s, p, o);
    }
    /**
     * @function save
     * @memberof RelationalMap
     * @instance
     * @description Saves the triples of this relation map text file in partial [Turtle](https://en.wikipedia.org/wiki/Turtle_(syntax) syntax.
     * @param {String} filename The output file name.
     */
    void save(String filename) const;
  };
}

#endif
