#include "RelationalMap.hpp"
#include "Binding.hpp"
#include "LValue.hpp"
#include "file.hpp"
#include "regex.hpp"

namespace macrovsa {
  RelationalMap& RelationalMap::add(const Symbol& subject, const Symbol& predicate, const Symbol& object)
  {
    Binding binding_po(predicate, object), binding_spo(subject, binding_po);
    Bundling::add(binding_spo);
    return *this;
  }
  RelationalMap& RelationalMap::add(const RelationalMap& map)
  {
    for(auto it = map.Bundling::get().cbegin(); it != map.Bundling::get().cend(); it++) {
      const Binding& binding_spo = dynamic_cast < const Binding& > (it->second);
      const Binding& binding_po = dynamic_cast < const Binding& > (binding_spo.x());
      const Symbol& subject = binding_spo.y(), & predicate = binding_po.y(), & object = binding_po.x();
      add(subject, predicate, object);
    }
    return *this;
  }
  RelationalMap& RelationalMap::add(String filename)
  {
    wjson::Value triples = wjson::LValue::readTriples(aidesys::load(filename));
    for(unsigned int i = 0; i < triples.length(); i++) {
      add(triples.at(i).at(0).asString(), triples.at(i).at(1).asString(), triples.at(i).at(2).asString());
    }
    return *this;
  }
  std::string RelationalMap::asString() const
  {
    std::string result = wjson::quote(getName()) + ": {\n";
    for(auto it = Bundling::get().cbegin(); it != Bundling::get().cend(); it++) {
      const Binding& binding_spo = dynamic_cast < const Binding& > (it->second);
      const Binding& binding_po = dynamic_cast < const Binding& > (binding_spo.x());
      const Symbol& subject = binding_spo.y(), & predicate = binding_po.y(), & object = binding_po.x();
      result += "  [ " + subject.asString() + predicate.asString() + " " + object.asString() + " ]\n";
    }
    return result + "}" + asStringTail() + "\n";
  }
  std::string RelationalMap::asString(const Symbol& subject, const Symbol& predicate, const Symbol& object)
  {
    Binding binding_po(predicate, object), binding_spo(subject, binding_po);
    return "[ " + subject.asString() + " " + predicate.asString() + " " + object.asString() + " ]" + binding_spo.asStringTail();
  }
  void RelationalMap::save(String filename) const
  {
    class TripleWriter {
      std::string filename;
      std::string prefixes = (std::string)
                             "\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ." +
                             "\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .";
      std::string triples;
      String quoteTurtleSymbol(const Symbol& symbol)
      {
        static std::string string;
        string = symbol.asString();
        if(aidesys::regexMatch(string, "^[a-z]*:.*")) {
          String prefix = aidesys::regexReplace(string, "^([^:]*):.*", "$1");
          if(prefixes.find("\n@prefix " + prefix + ":") == std::string::npos) {
            prefixes += "\n@prefix " + prefix + ": <file://" + filename + "#" + prefix + "> .";
          }
        } else {
          string = "\"" + aidesys::regexReplace(string, "\"", "\"\"\"") + "\"";
        }
        return string;
      }
public:
      void save(String filename, const RelationalMap& map)
      {
        this->filename = filename;
        for(auto it = map.Bundling::get().cbegin(); it != map.Bundling::get().cend(); it++) {
          const Binding& binding_spo = dynamic_cast < const Binding& > (it->second);
          const Binding& binding_po = dynamic_cast < const Binding& > (binding_spo.x());
          const Symbol& subject = binding_spo.y(), & predicate = binding_po.y(), & object = binding_po.x();
          triples += quoteTurtleSymbol(subject) + " " + quoteTurtleSymbol(predicate) + " " + quoteTurtleSymbol(object) + " .\n";
        }
        aidesys::save(filename, "@base <file://" + filename + "#> ." + prefixes + "\n" + triples);
      }
    }
    tripleWriter;
    tripleWriter.save(filename, *this);
  }
}
